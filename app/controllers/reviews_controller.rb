class ReviewsController < ApplicationController
  
  def create
    place = Place.find(params[:place_id])
    review = place.reviews.new review_params
    review.user = current_user
    review.save
    redirect_to place_path(place)
  end
  
  def destroy
    place = Place.find(params[:place_id])
    review = place.reviews.find(params[:id]) 
    review.destroy
    respond_to do |format|
      format.html {redirect_to place_path(place), notice: 'Review was successfully destroyed'}
    end
  end

  private
  
    def review_params
      params.require(:review).permit(:food, :service, :interior, :body)
    end

end