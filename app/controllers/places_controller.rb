class PlacesController < ApplicationController
  before_action :set_place, only: [:show, :edit, :update, :destroy]
  before_action :ensure_login!, only: [:new, :create, :edit, :update]
  def index
    if params[:category]
      @places = Place.where(:category => params[:category]).where(accepted: true).paginate(:page => params[:page])
    else
      @places = Place.where(accepted: true).paginate(:page => params[:page])
    end
    @categories = Category.all
  end

  def show
    @food_average = @place.reviews.average(:food)
    @food_average = @food_average.round(1) if @food_average
    @service_average = @place.reviews.average(:service)
    @service_average = @service_average.round(1) if @service_average
    @interior_average = @place.reviews.average(:interior)
    @interior_average = @interior_average.round(1) if @interior_average
    
    all_average = [@food_average, @service_average, @interior_average]
    if all_average == [nil, nil, nil]
      @overall_average = 0
    else
      @overall_average = (all_average.sum / all_average.size.to_f).round(1) if all_average
    end
  end

  def new
    @place = Place.new
  end

  def edit
  end

  def create
    @place = Place.new(place_params)
    @place.user = current_user
    respond_to do |format|
      if @place.save
        format.html { redirect_to @place, notice: 'Place was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @place.update(place_params)
        format.html { redirect_to @place, notice: 'Place was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @place.destroy
    respond_to do |format|
      format.html { redirect_to places_url, notice: 'Place was successfully destroyed.' }
    end
  end

  private
    def set_place
      @place = Place.find(params[:id])
    end

    def place_params
      params.require(:place).permit(:name, :user_id, :category_id, :agreement, :image, :accepted, :description)
    end

    def ensure_login!
      unless user_signed_in?
        redirect_to root_path
      end
    end

end
