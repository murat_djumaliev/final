class AdminController < ApplicationController
  before_action :ensure_admin!
  def index
    
  end

  def places
  	@places = Place.all
  end

  def accept_place
    place = Place.find(params[:place])
    place.accepted = true
    respond_to do |format|
      if place.save
        format.html { redirect_to admin_places_url }
      else
        format.html { redirect_to admin_places_url }
      end
    end  
  end

  private

   def ensure_admin!
      unless current_user && current_user.admin?
        redirect_to root_path
      end
    end


end