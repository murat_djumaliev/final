class Review < ApplicationRecord
  belongs_to :user
  belongs_to :place

  validates :food, presence: true
  validates :service, presence: true
  validates :interior, presence: true
  validates :body, presence: true
end
