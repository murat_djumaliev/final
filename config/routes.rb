Rails.application.routes.draw do
 
  devise_for :users
  
  resources :categories
  
  resources :places do 
    resources :reviews 
  end
  
  namespace :admin do
    get 'index'
    get 'places'
    get 'accept_place'
  end
  
  root 'places#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
