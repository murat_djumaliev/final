# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


superadmin = User.create!(name:'superadmin', email:'superadmin@example.com',
password: 'superadmin', password_confirmation:'superadmin', admin:true)
murat = User.create(name:'Murat', email:'murat@example.com',
password:'123456',password_confirmation:'123456')

category1 = Category.create!(name: 'Bar')
category2 = Category.create!(name: 'Resto')
category3 = Category.create!(name: 'Launge')

place1 = Place.create!(name:'NoName', description:'bokonbaeva-sovetskaya',
category_id:1, user_id: 1, agreement:true, accepted:true, image: File.new("app/assets/images/3.png"))
place2 = Place.create!(name:'Urban', description:'bokonbaeva-razakova',
category_id:2, user_id: 2, agreement:true, image: File.new('app/assets/images/1.jpeg'))

review1 = Review.create!(food:'5', service:'4', interior:'3', place_id:1,
user_id:1, body:'Nice Place')
review2 = Review.create!(food:'3', service:'2', interior:'2', place_id:1,
user_id:2, body:'Ok Place')
review3 = Review.create!(food:'2', service:'1', interior:'4', place_id:2,
user_id:1, body:'Nice Place')
review4 = Review.create!(food:'3', service:'2', interior:'2', place_id:2,
user_id:2, body:'Ok Place')