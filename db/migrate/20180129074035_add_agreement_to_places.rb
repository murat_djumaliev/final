class AddAgreementToPlaces < ActiveRecord::Migration[5.1]
  def change
    add_column :places, :agreement, :boolean
  end
end
