class AddAcceptedToPlaces < ActiveRecord::Migration[5.1]
  def change
    add_column :places, :accepted, :boolean, default: :false
  end
end
